import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ShelterService} from '../../app/services/shelter.service';
/**
 * Generated class for the ShelterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-shelter',
  templateUrl: 'shelter.html',
})
export class ShelterPage {
	
  	user: any;
	shelter: any;
	reviews: Array<any> = [];

  	constructor(
  		public navCtrl: NavController,
  		public navParams: NavParams,
  		public shelterService: ShelterService
  		) {
  		this.getShelterDetails();
  	}

  	ionViewDidLoad() {
    	console.log('ionViewDidLoad ShelterPage');
  	}

  	getShelterDetails() {
		
		let param = { shelter_id: this.navParams.data.shelter_id};

		this.shelterService.getShelter(param).then( 
			
			
			res => {
			
				this.shelter = res;
				
		}).catch((error) => {
				console.log(error);
		});
		
	}

}
